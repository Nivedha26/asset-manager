package com.example.demo.entity;

import java.sql.Timestamp;

public class Stock {

	private int orderId;
	private String ticker;
	private Timestamp dateTime;
	private int volume;
	private double valuePerStock;
	private String buyOrSell;
	
	public Stock(int orderId, String ticker, Timestamp dateTime, int volume, double valuePerStock, String buyOrSell) {
		super();
		this.orderId = orderId;
		this.ticker = ticker;
		this.dateTime = dateTime;
		this.volume = volume;
		this.valuePerStock = valuePerStock;
		this.buyOrSell = buyOrSell;
	}
	
	public Stock() { }
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public Timestamp getDateTime() {
		return dateTime;
	}
	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public double getValuePerStock() {
		return valuePerStock;
	}
	public void setValuePerStock(double valuePerStock) {
		this.valuePerStock = valuePerStock;
	}
	public String getBuyOrSell() {
		return buyOrSell;
	}
	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}
	
}
